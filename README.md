# FAIR-Data Community Droogte en Wateroverlast (DenW)

## Officiele repository 
Officiele repository voor de data plan template, metadata template, FAIR-evalautor van de DenW FAIR-Data community is https://gitlab.com/fair-data-communities/denw/assets

## Omschrijving
Deze repository bevat de open source assets voor data aanbieders van de FAIR data community 'DenW' (Droogte en Wateroverlast). De oorspronkelijke locatie van deze repository is https://gitlab.com/fair-data-communities/denw/assets

## DenW Data plan template
Het data plan template is gemaakt in JSON. De template bevat de DenW Community specifieke data plan vragen. Je kan deze file gebruiken als seeding file en als databron. Locatie: https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/dataplan_templates/dataplan_template.json 

## DenW Metadata template
Coming soon

## DenW FAIR-Data Evaluator 
Coming soon

## Open Source Licentie
https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/LICENSE 

## Gebruikerslicentie
Alle data-aanbieders en datagebruikers binnen de DenW FAIR-Data Community gaan met de volgende gebruikerslicentie akkoord: https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/licenses/user_license.md

## Community Beheer
Het community beheer van deze open source assets, geintroduceerd voor de FAIR-Data Community Droogte en Wateroverlast (DenW), wordt gedaan door de Purple Polar Bear B.V.

## Contributie
Zie de contributing file: https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/CONTRIBUTING

## Versies
De officiele versies worden gekenmerkt door tags die het formaat hebben van: release_jaartal. Daarnaast bevat master altijd de laatste gereleasde versie. Doorlopende ontwikkelingen vinden plaats in de development branch.


