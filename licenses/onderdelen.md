# Onderdelen

## Templates
1. Voor het implementeren van de data aanbiedende kant worden de onderdelen gebruikt die hieronder bij 2 benoemd worden.
2. De volgende templates zijn nodig:
  1. Denw Data Plan Template. Deze kan gevonden worden op https://gitlab.com/fair-data-communities/denw/assets/-/tree/master/dataplan_templates
  2. DenW Metadata Template. Deze kan gevonden worden op https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/definitions/denw.rdf
  3. De aangeboden assets data worden gecheckt op bruikbaarheid middels de DenW FAIR-Data Evaluator.

### FAIR-Score 
1. De aangeboden data worden gecheckt op bruikbaarheid middels de DenW
FAIR-Data Evaluator.
2. De FAIR-score van deze evaluatie moet minstens XX% zijn, zodat de rest van de
community de data goed kan interpreteren.
3. De FAIR-score moet erkend worden door middel van een GO FAIR Foundation
certificaat.

### DenW FAIR-Data Evaluators
1. De DenW FAIR-Data Evaluator wordt jaarlijks vernieuwd.
2. Deelnemers moeten tenminste voldoen aan huidige versie – 2 jaar.
