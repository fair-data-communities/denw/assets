# DE ONDERGETEKENDEN,

1. Een bronhouder die haar data aanbiedt en daardoor deelt met andere organisaties, hierna te
noemen data-aanbieder.
2. Een organisatie die gebruik maakt van de data die gedeeld wordt door bronhouders, hierna
te noemen datagebruiker.

hierna gezamenlijk aangeduid als Partijen,

# ZIJN OVEREENGEKOMEN ALS VOLGT:

#### Artikel 1 – Begrippen

1.	Onder “Overeenkomst” wordt verstaan: deze geheimhoudingsovereenkomst, met eventuele bijlagen.

1.	Onder “Informatie” wordt verstaan:

a)	alle kennis en informatie van welke aard dan ook, hetzij mondeling verstrekt, hetzij verstrekt in de vorm van een schriftelijk of digitaal document (met inbegrip van gespreksverslagen, notities, rapporten, presentaties, spreadsheets, tekeningen, foto’s, computerprogramma’s en andere informatiedragers in welke vorm dan ook), die datagebruiker direct of indirect van data-aanbieder ontvangt in verband met de Levering dan wel in verband met de voorbereiding van de Levering en/of het opstellen van een offerte voor het verwerven van een opdracht daartoe, een en ander voor zover deze kennis en/of informatie op het moment van het ontvangst niet:

b)	publiekelijk bekend was of deel uitmaakte van de publiekelijk toegankelijke literatuur of andere publiekelijk toegankelijke informatiedragers; of

c)		reeds door data-aanbieder zelf publiekelijk is geopenbaard.

#### Artikel 2 – Geheimhoudingsverplichtingen

1.	De datagebruiker zal de Informatie slechts gebruiken voor de uitvoering of voorbereiding van de Levering en/of voor het opstellen van een offerte voor het verwerven van een opdracht daartoe.

1.	De Informatie is en blijft eigendom van data-aanbieder.

1.	De datagebruiker zal de Informatie (of enig deel daarvan) niet reproduceren, kopiëren, toegankelijk maken, beschikbaar stellen aan en/of gebruiken ten behoeve van een of meer derde(n).

1.	De datagebruiker is verplicht:

a)	alle redelijke maatregelen te nemen voor een veilige opslag van de Informatie;

b)	de Informatie niet langer onder zich te houden dan redelijkerwijs noodzakelijk is voor de nakoming van de verplichtingen uit deze Overeenkomst en de Informatie te na overleg met data-aanbieders te vernietigen dan wel de Informatie aan data-aanbieders te retourneren

- op eerste verzoek van data-aanbieders; dan wel 

- onmiddellijk na het afbreken van onderhandelingen ter verwerving van een opdracht; dan wel

- na volledige nakoming van de uit de Levering voortvloeiende verplichtingen, met inachtneming van eventueel daarin vermelde termijnen met betrekking tot garanties en dergelijke;

c)	de verplichtingen in verband met de Levering, de voorbereiding daarvan of het uitbrengen van een offerte voor het verwerven van een opdracht daartoe uitsluitend te doen uitvoeren door personen waarvan datagebruiker in redelijkheid meent dat zij betrouwbaar zijn en de Informatie alleen aan die medewerker(s) van de eigen onderneming of van enige door datagebruiker ingeschakelde derde(n) ter beschikking te stellen voor wie zulks noodzakelijk is voor de realisering van het doel waarvoor de Informatie is verstrekt.

1.	Datagebruiker zal ervoor zorgen dat alle medewerkers en/of door haar ingeschakelde derden die op enigerlei wijze betrokken zijn bij de uitvoering of voorbereiding van de Levering en/of bij het opstellen van een offerte voor het verwerven van een opdracht daartoe, op de hoogte zijn van de bepalingen van deze Overeenkomst en dat zij zich daaraan zullen houden. Data-aanbieder kan verlangen dat bedoelde medewerkers en/of ingeschakelde derden separaat (een duplicaat van) deze Overeenkomst ondertekenen.

1.	Indien datagebruiker reden heeft om aan te nemen dat haar medewerker(s) of de door haar ingeschakelde derde(n) op ongeoorloofde wijze gebruikt maakt/maken van de Informatie, zal de datagebruiker dit gebruik onverwijld doen stoppen. Datagebruiker zal data-aanbieder onmiddellijk schriftelijk op de hoogte stellen van het ongeoorloofde gebruik en zal data-aanbieder daarbij voorzien van zoveel mogelijk informatie omtrent het ongeoorloofde gebruik.

1.	Indien openbaarmaking van Informatie door de ontvangende partij vereist is op grond van een wettelijke verplichting, een gerechtelijk bevel of een bevel van een bevoegde instantie, zal de ontvangende partij (voor zover wettelijk en praktisch mogelijk) de verstrekkende partij onmiddellijk op de hoogte stellen van de omstandigheden en de aard van de te onthullen Informatie en de verstrekkende partij een redelijke gelegenheid bieden om op kosten van de verstrekkende partij de openbaarmaking van Informatie te beperken. De ontvangende partij zal alle informatie en bijstand verlenen waar de verstrekkende partij in verband hiermee redelijkerwijs om verzoekt. Indien de ontvangende partij niettemin verplicht is om Informatie van de verstrekkende partij openbaar te maken, zal de ontvangende partij de openbaarmaking ervan beperken tot die informatie die vereist is volgens de toepasselijke wetgeving.

1.	Onverminderd het recht van data-aanbieder op nakoming van datagebruiker verplichtingen en onverminderd het bepaalde in artikel 3.1 van deze Overeenkomst, is datagebruiker aansprakelijk voor iedere vorm van schade die data-aanbieder lijdt als gevolg van het overtreden door datagebruiker en/of haar medewerker(s) en/of enige voor datagebruiker werkzame derde van enige bepaling in deze Overeenkomst.

#### Artikel 3 – Boete

1.	Datagebruiker verbeurt van rechtswege een onmiddellijk opeisbare boete van € 25.000 voor iedere overtreding van een bepaling uit deze Overeenkomst, tenzij datagebruiker kan aantonen dat de overtreding niet aan haar te wijten was.

1.	Het bepaalde in artikel 3.1 laat onverlet het recht van data-aanbieder op nakoming van de verplichtingen van datagebruiker en het recht op schadevergoeding als bedoeld in artikel 2.7 van deze Overeenkomst.

1.	Indien data-aanbieder schade lijdt als gevolg van enige overtreding van een bepaling uit deze Overeenkomst, zal de schadevergoeding waar data-aanbieder dientengevolge recht op heeft, worden verrekend met het bedrag van de door datagebruiker betaalde boete als bedoeld in artikel 3.1 met betrekking tot diezelfde overtreding.

#### Artikel	 4 – Duur van de verplichtingen

Deze Overeenkomst gaat in op het moment dat zij door beide Partijen is ondertekend en zal gelden voor de duur van vijf jaren, tenzij met de uitvoering van de datagebruiker – indien overeengekomen – een langere periode is gemoeid. Alsdan zal deze Overeenkomst van kracht zijn tot vijf jaar na beëindiging van de aan de Levering verbonden werkzaamheden. Beëindiging van de verplichtingen uit deze Overeenkomst kan ook plaatsvinden indien en voor zover de Informatie: 

a)	publiekelijk bekend wordt of deel gaat uitmaken van de publiekelijk toegankelijke literatuur zonder schending van enige geheimhoudingsplicht door datagebruiker;

b)	door enige derde (anders dan enige persoon handelend namens data-aanbieder) aan datagebruiker bekend is gemaakt en deze derde vrijelijk over de Informatie mocht beschikken zonder schending van enige geheimhoudingsplicht;

c)	volledig is vernietigd of is terugverkregen door data-aanbieder en data-aanbieder de beëindiging van deze Overeenkomst heeft bevestigd.

#### Artikel 5 – Toepasselijk recht en geschillen

Deze Overeenkomst wordt beheerst door Nederlands recht. Geschillen die tussen Partijen ontstaan naar aanleiding van deze Overeenkomst zullen worden voorgelegd aan de bevoegde rechter te Rotterdam.
 
