# DenW Samenwerkingsovereenkomst  

Dit document kan gevonden worden op https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/licenses/user_license.md

Dit document bevat versie 0.1 van de DenW (Droogte en Wateroverlast) Samenwerkingsovereenkomst

Deze licentie en haar onderdelen zelf vallen onder de licentie zoals gespecificeerd in https://gitlab.com/fair-data-communities/denw/assets/-/blob/licence/LICENSE. Dit betekent dus dat het licentie document en de daaraan gerelateerde documenten zelf onder deze licentie vallen.

## FAIR-Data Community: DenW
Dit document is bedoelt om een aantal hygiëne afspraken te maken tussen de deelnemers van de DenW FAIR-Data Community. Dit document is een lopend document en is op dit moment vooral bedoelt om intentie weer te geven en niet om zeer stringente afspraken te maken. We hebben voor de toekomst een eeste opzet gemaakt, dit is niet in beton gegoten. Dit wordt gezien als een beginpunt voor het uitbouwen van deze FAIR-Data samenwerking.

In deze gebruikerslicentie staat beschreven hoe zowel data-aanbieders als datagebruikers om
dienen te gaan met de gedeelde data binnen de DenW FAIR-Data community. Er wordt
vastgesteld hoe de data onderling aangeboden dient te worden en hoe er onderling op
verantwoordelijke wijze met de data omgegaan wordt.
Een FAIR-Data community deelnemer is veelal zowel data-aanbieder als datagebruiker van de
data. Daarnaast kunnen externe bedrijven ook ingeschakeld worden om met de gedeelde
data te werken. In beide gevallen betekent dit dat de inzage van de gegevens door andere
organisaties gedaan wordt dan de organisatie die bronhouder is van de gegevens. Bij inzage
van andermans gegevens, dient iedereen zich te houden aan deze gebruikerslicentie.
Deze gebruikerslicentie wordt gezamenlijk opgesteld en is geldig tot drie maanden nadat een
nieuwe versie van de gebruikerslicentie is uitgebracht. Een nieuwe versie wordt aan alle
leden van de DenW FAIR-Data Community voorgelegd.

## Begrippen
1. Onder samenwerkingsovereenkomst wordt verstaan: dit document en de daaraan gerelateerde documenten. Dit zijn:
[Beveiligingsdocument](https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/licenses/beveiliging.md),
[Onderdelen](https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/licenses/onderdelen.md),
[Geheimhoudingsovereenkomst](https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/licenses/geheimhoudingsovereenkomst.md),
[Aansluitingsvoorwaarden](https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/licenses/aansluitingsvoorwaarden.md),
[Audit voorwaarden](https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/licenses/audit_voorwaarden.md).
2. Voor de gerelateerde documenten geldt dat er akkoord gegaan wordt met een versie die overeenkomt met de versie van dit document.

## Gebruikersgroepen
1. Een bronhouder die haar data aanbiedt en daardoor deelt met andere organisaties, hierna te
noemen data-aanbieder.
2. Een organisatie die gebruik maakt van de data die gedeeld wordt door bronhouders, hierna
te noemen datagebruiker.

Bovenstaande partijen gaan akkoord met de DenW [geheimhoudingsovereenkomst](https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/licenses/geheimhoudingsovereenkomst.md).

## Data-aanbieders
#### 1. Eigendom
1. Data die vanuit de bron van de data-aanbieder gedeeld wordt, is en blijf te allen tijde
eigendom van de data-aanbieder. 
2. Data wordt aangeboden middels de FAIR-werkwijze die FAIR-Data Community DenW heeft
vastgesteld (ookwel de Paarse-Werkwijze genoemd), hiervoor worden een aantal open source onderdelen gebruikt. Deze onderdelen worden beschreven in [onderdelen](https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/licenses/onderdelen.md).

#### 2. Terugtrekking data-aanbieder FAIR-Data Community
1. Iedere deelnemer van de DenW FAIR-Data Community kan op elk moment, zonder reden
of commentaar, haar data-aanbieding stopzetten.
6. Dit kan door de acceptatie van inkomende certificaten (SSL handshakes) op het
aanbiedende endpoint stop te zetten. Hierna kan geen enkele organisatie de
data meer raadplegen.
7. Om performance redenen kan de data nog ergens gecached zijn, in lid 10 wordt
beschreven hoe datagebruikers hiermee om moeten gaan.
8. De tijdslimieten komen tot stand vanaf het moment dat de terugtrekkende
organisatie de datagebruikers op de hoogte heeft gesteld van terugtrekking uit
de DenW FAIR-Data Community. De datagebruikers moeten het
terugtrekkingsproces voor eindapplicaties uitvoeren binnen de afgesproken tijd,
besproken in lid 13.

#### 3. Systemen data-aanbieder FAIR-Data Community
1. De systemen van data-aanbieders binnen deze FAIR-Data Community dienen 99%
beschikbaar te zijn tijdens werkdagen.
7. DenW FAIR-Data Community deelnemers kunnen redelijke hoeveelheid databevragingen
aan en dragen er zorg voor dat de service dit aan kan.
8. Redelijkheid wordt door de data-aanbieder bepaald, indien onredelijkheid
wordt waargenomen kan de data-aanbieder te allen tijde de toegang tot haar
brondata aan een bepaalde datagebruiker verbieden door het certificaat niet
meer te accepteren.

#### 4. Jaarlijkse FAIR-Data Community verbreding
Jaarlijks zal de gedeelde data voor de DenW FAIR-Data Community besproken worden. Hierbij wordt besloten of er meer data met elkaar gedeeld kan/moet worden ter
bevordering van de operatie en wordt de huidige gedeelde data geëvalueerd. Alle
deelnemers van de DenW FAIR-Data Community worden hierbij betrokken. De DenW FAIR-Data Community wordt hierbij ondersteund door Purple Polar Bear. 

## Datagebruikers
#### 5. Eigendom
1. De bronhouder is en blijft te allen tijde eigenaar van de data.

#### 7. Verbod gegevens op te slaan
1. Het wordt datagebruikers verboden gegevens van bronhouders, genoemd data-
aanbieders, op wat voor een wijze dan ook elders op te slaan.

#### 8. Verbod gegevens te kopieren
1. Gegevens van data-aanbieders mogen niet gekopieerd worden in welke vorm dan ook.
Het is bijvoorbeeld verboden de data in een andere database op te slaan, naar Excel te
exporteren etc.

#### 9. Bewust gebruik 
1. Datagebruikers dragen er zorg voor dat mensen die gedeelde data van de DenW FAIR-Data Community gebruiken en/of inzien hier toereikend voor opgeleid worden. Deze mensen moeten onderandere op de hoogte gesteld worden van deze samenwerkingsovereenkomst. De DenW FAIR-Data connectoren zullen uitgerust worden met een stuk user management, zodat een kleine groep mensen toegang kan krijgen tot de gedeelde data. Hierdoor blijft het aantal mensen die opgeleid moeten worden beheersbaar.  

#### 10. Dataopslag voor performance redenen datagebruiker
1. Indien een datagebruiker om performance redenen de data opslaat in welke vorm dan
ook dient de datagebruiker deze dataopslag voor de laatste dag van iedere maand te
verwijderen.

#### 11. Informatielek; niet open data
1. Indien een informatielek plaatsvindt dient de datagebruiker dit binnen 24 uur aan alle data-
aanbieders te melden.
9. Deze melding dient met het duidelijke onderwerp: DenW DATALEK” verstuurd
te worden.

#### 12. Audit datagebruiker
1. De community kan een audit laten uitvoeren op basis van de [audit voorwaarden](https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/licenses/audit_voorwaarden.md). Deze audit wordt uitgevoerd bij datagebruikers om te kijken
of het datagebruik conform de intentie van deze gebruikersovereenkomst plaatsvindt.
9. De uitslag van de audit kan gebruikt worden een datagebruiker af te sluiten
van de DenW FAIR-Data Community.

#### 13. Terugtrekking data-aanbieder
1. Een data-aanbieder kan via de mail kenbaar maken zich terug te trekken uit de DenW FAIR-Data Community vanaf een terugtrekkingsdatum.
2. Vanaf de terugtrekkingsdatum dient de data die de data-aanbieder vanuit
haar bron heeft aangeboden op alle plekken van de datagebruiker verwijderd te worden.
3. Het verwijderen van deze data dient binnen 168 uur na de terugtrekkingsdatum gedaan te zijn. 
4. De bronhouder is en blijft te allen tijde eigenaar van de data.

#### 14. Regels omtrent serverbelasting datagebruikers
1. Server van data-aanbieders mag bevraagd worden met inachtneming van de fair use
policy. Hierbij wordt van moderne en efficiënte technieken uitgegaan.
9. Dit betekent dat een datagebruiker niet onredelijk veel databevragingen mag
doen per bronhouder, oftewel data-aanbieder.
9. Indien onredelijkheid wel het geval is mag de data-aanbieder te allen tijde de
toegang tot haar brondata aan een bepaalde datagebruiker verbieden door het
certificaat niet meer te accepteren.

## Security
#### 15. Toegang en Beveiliging
1. De beveiliging staat beschreven in het [beveiligingsdocument](https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/licenses/beveiliging.md). Data aanbieders en/ of datagebruikers gaan akkoord met deze versie of een hogere versie van het beveiligingsdocument.

## FAIR-Data Community Bestuur
#### 16. Nieuwe data-aanbieders en datagebruikers
1. Eventueel nieuwe deelnemers (data-aanbieders) en/of datagebruikers van de DenW FAIR-
Data Community worden door J. Maaskanr werkzaam bij Informatiehuis Water en trekker van de DenW
FAIR-Data Community schriftelijk geaccepteerd. De schriftelijke acceptatie wordt door middel van de [aansluitingsvoorwaarden](https://gitlab.com/fair-data-communities/denw/assets/-/blob/master/licenses/aansluitingsvoorwaarden.md) vastgesteld.
2. De trekker van de DenW FAIR-Data Community wordt op technische niveau ondersteund
door K. Boersma werkzaam bij Informatiehuis Water.

#### 17. Schorsing datagebruikers
1. Het bestuur kan datagebruikers schorsen op grond van een audit waarin beoordeeld
wordt of de datagebruiker de data die wordt gedeeld binnen deze FAIR-Data Community
gebruikt conform de intentie van deze gebruikersovereenkomst.
2. Indien er overgegaan wordt op schorsing moet de datagebruiker binnen 120
uur na aankondiging alle data in haar systemen verwijderd hebben. 
3. De verantwoordelijkheid voor data vernietigingen ligt bij de datagebruiker,
zowel direct als indirect.

#### 18. Belangenverstrengeling (!discussiepunt volgens overleg)
1. Belangenverstrengeling is bij de DenW FAIR-Data Community verboden. De data-
aanbiedende kant wordt niet vervlochten met data gebruikende kant. Hierdoor wordt
vendor lock-in en commercieel voordeel voorkomen.
9. Voorbeeld: Indien een organisatie een eindapplicatie maakt (Dashboards, user
interfaces voor processen etc) dan zal die organisatie niets met de
datavoorziening van doen hebben.
9. Voorbeeld: Indien een organisatie helpt bij de datavoorziening zal die partij
geen eindapplicaties maken.

#### 19. Tussensystemen
1. Tussensystemen mogen niet gebruikt worden als basissysteem voor andere systemen,
waardoor een ander systeem dan de bron als brondata kan worden aangezien. Hiermee is
het de bedoeling Piramidevormig, en ook vendor lock-in, te voorkomen.

#### 20. Ondersteuning
1. De DenW FAIR-Data Community wordt ondersteund door Purple Polar Bear
