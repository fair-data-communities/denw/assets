# Beveiliging

1. Om de veiligheid van de gegevens die tussen een client (datagebruiker) en server (data-
aanbieder) worden overgedragen te garanderen, wordt er voor de DenW FAIR-Data
Community een dubbele SSL-verbinding geïmplementeerd.
2. In het geval van een dubbele-SSL verbinding authentiseren zowel de client
(datagebruiker) als de server (data-aanbieder) elkaar om ervoor te zorgen dat
beide partijen die bij de communicatie betrokken zijn, worden vertrouwd. Beide
partijen delen hun publieke certificaten met elkaar en vervolgens wordt op basis
daarvan verificatie / validatie uitgevoerd. Dit zullen alle bronhouders in het
geval van beveiligde data zo doen, bronhouders met open data hoeven dit
uiteraard niet te doen.

#### Stappen
De stappen die nodig zijn bij het tot stand brengen van een verbinding en
gegevensoverdracht tussen een client (datagebruiker) en server (data-aanbieder) met
een dubbele-SSL verbinding:
1. De cliënt vraagt een beschermde bron aan via het HTTPS-protocol en het SSL-
handshakeproces begint.
1. De server stuurt zijn openbare certificaat terug naar de client, samen met
server “Hello world”.
1. Client valideert / verifieert het ontvangen certificaat. Client verifieert het
certificaat via certificeringsinstantie (CA) voor door CA ondertekende
certificaten.
1. Als het servercertificaat met succes is gevalideerd, zal de client zijn openbare
certificaat aan de server verstrekken.
De DenW FAIR-Data Community wordt ondersteund door Purple Polar Bear17.5. Server valideert / verifieert het ontvangen certificaat. De server verifieert het
certificaat via de certificeringsinstantie (CA) voor door de CA ondertekende
certificaten.
1. Na voltooiing van het handshakeproces, communiceren client en server met
elkaar en dragen ze gegevens over die versleuteld zijn met de geheime sleutels
die tijdens de handshake tussen de twee worden gedeeld.
1. Zie onderstaande tekening ter verduidelijking

![Beveiligingsplaat](licenses_security.png)

#### CA 
1. Je kunt het certificaat verkrijgen via de organisatie die de CA beheert. Deze organisatie is
door de DenW FAIR-Data Community aangewezen.
Security Audit
19. De implementatie van een dubbele SSL-verbinding kan op verzoek van het DenW Bestuur
voor zowel de data-aanbieder als de datagebruiker ge-audit worden.
